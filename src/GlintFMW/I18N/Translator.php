<?php declare(strict_types=1);
	namespace GlintFMW\I18N;

	/**
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\I18N
	 */
	interface Translator
	{
		/**
         * @return string The current language to be used for the localization system
         */
		function detectLanguage (): string;
	};