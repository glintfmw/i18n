<?php declare (strict_types=1);
	namespace GlintFMW\I18N\Configuration;

	use GlintFMW\Dependencies\Injector;
    use GlintFMW\I18N\Exception\LocalizationMissingException;

	use GlintFMW\Configuration\Providers\Map;
	use GlintFMW\Configuration\Exception\IntegrityException;

    use GlintFMW\I18N\TranslationService;
    use GlintFMW\I18N\Translator;

	/**
	 * Configuration provider for the localization system
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\I18N\Configuration
	 */
	class LocalizationProvider extends Map
	{
	    /** @var string The default folder where to save the language files */
	    const DEFAULT_LOCALIZATION_OUTPUT_FOLDER = 'dist/lang';
	    /** @var array<string, array <string, string>> The list of translations available for the user */
		private array $localizationData = array ();
		/** @var Translator Translator to use for detecting the user's language */
		private Translator $translator;

		public function __construct (array $configuration, Injector $dependencyInjector)
        {
            parent::__construct ($configuration, $dependencyInjector);

            $translator = $this->dependencyInjector->inject ($configuration ['translator']);

            if ($translator instanceof Translator == false)
                throw new \Exception ("The class {$configuration ['translator']} must be a subclass of " . Translator::class);

            $this->translator = $translator;

            $this->dependencyInjector->registerDependency (
                new TranslationService ($this, $this->dependencyInjector)
            );
        }

        /**
         * Checks if the given language is available to be used
         *
         * @param string $language
         * @return bool
         */
        public function isLanguageAvailable (string $language): bool
        {
            return in_array ($language, $this->getLanguages (), true) === true;
        }

        /**
         * Loads the localization data for the given language
         *
         * @param string $lang
         * @throws LocalizationMissingException
         */
        private function loadLocalizationData (string $lang): void
        {
            $file = $this->getLocalizationFolder () . '/' . $lang . '.php';

            if (file_exists ($file) === false)
            {
                throw new LocalizationMissingException ("The localization file {$file} for language {$lang} doesn't exist");
            }

            $this->localizationData [$lang] = require $file;
        }

		static function provides () : string
		{
			return "appLocalization";
		}

		/** @return string The path to the localization folder */
		private function getLocalizationFolder () : string
		{
		    return $this->getConfiguration () ['languagePath'];
		}

		/**
         * Performs checks on the configuration and produces the actual settings
         * to be used on runtime
         *
         * @param array<int|string, mixed> $input The configuration data available on the JSON
         *
         * @return array{defaultLang:string,languagePath:string,list:array<string>,translator:class-string<Translator>}
         *
         * @throws \GlintFMW\Configuration\Exception\IntegrityException When the configuration is not valid
         */
		static function convert (array $input): array
		{
			if (array_key_exists ('defaultLang', $input) === false)
			    throw new IntegrityException ("A default language must be specified");
			if (array_key_exists ('translator', $input) === false)
			    throw new IntegrityException ("A translator must be specified");

			/** @var string $languageOutputPath */
			$languageOutputPath = $input ['outputDir'] ?? self::DEFAULT_LOCALIZATION_OUTPUT_FOLDER;
            $languageOutputPath = getcwd () . '/' . trim ($languageOutputPath, '/\\');

            if (file_exists ($languageOutputPath) === false)
                mkdir ($languageOutputPath, 0750, true);
            if (is_dir ($languageOutputPath) === false)
                throw new IntegrityException ("Output path for localization files '$languageOutputPath' is not a folder");

            $languageList = array ();

            foreach ($input ['langs'] as $code => $file)
            {
                $fullpath = getcwd () . '/' . trim ($file, '/\\');

                if (file_exists ($fullpath) === false)
                    throw new IntegrityException ("The specified language file for $code doesn't exist");

                $fileContents = file_get_contents ($fullpath);

                if ($fileContents === false)
                    throw new IntegrityException ("Cannot open '$fullpath' for reading");

                $fileContents = json_decode ($fileContents, true);

                // export the contents to the final file
                file_put_contents (
                    $languageOutputPath . "/$code.php",
                    "<?php declare (strict_types=1);" . PHP_EOL .
                    "return " . var_export ($fileContents, true) . ';' . PHP_EOL
                );

                $languageList [] = $code;
            }

            // setup translator
            if (class_exists ($input ['translator']) === false)
                throw new IntegrityException ("The class {$input ['translator']} doesn't exist");
            if (is_subclass_of ($input ['translator'], Translator::class) === false)
                throw new IntegrityException ("The class {$input ['translator']} must be a subclass of " . Translator::class);

            if (class_exists ("GlintFMW\\Templating\\Configuration\\TemplatingProvider") === true)
                \GlintFMW\Templating\Configuration\TemplatingProvider::registerDefaultFunction ('_', 'translate@GlintFMW\\I18N\\TranslationService');

            return array (
                'defaultLang' => (string) $input ['defaultLang'],
                'languagePath' => $languageOutputPath,
                'list' => $languageList,
                'translator' => $input ['translator']
            );
		}

        /**
         * Obtains information and the translations for a given language
         *
         * @param string $lang The language to get data for
         *
         * @return array<string, string> The localization info for the given language
         * @throws LocalizationMissingException
         */
		function getLanguage (string $lang): array
		{
			if (array_key_exists ($lang, $this->localizationData) == false)
			{
			    $this->loadLocalizationData ($lang);
			}

			return $this->localizationData [$lang];
		}

        /**
         * Obtains the specified translation for the given $lang and $key
         *
         * @param string $lang The language to get info from
         * @param string $key The localization key
         *
         * @return string The translation text
         * @throws LocalizationMissingException When the message wasn't found in the specified language
         */
		function getTranslation (string $lang, string $key)
		{
			$data = $this->getLanguage ($lang);

			if (array_key_exists ($key, $data) == false)
			{
			    // try to load the translation from the default language
                // if this is not the default language
			    if ($lang == $this->getDefaultLanguage ())
			        return $this->getTranslation ($this->getDefaultLanguage (), $key);
			    else
				    throw new LocalizationMissingException ("The localization '{$key}' for lang '{$lang}' doesn't exist");
			}

			return $data [$key];
		}

		/** @return array<string> The list of localization languajes available */
		private function getLanguages (): array
		{
		    return $this->getConfiguration () ['list'];
		}

		/** @return string The current language in use */
		function getDefaultLanguage (): string
		{
		    return $this->getConfiguration () ['defaultLang'];
		}

        /** @return string The language currently active in the system */
		function getCurrentLanguage (): string
        {
            $activeLanguage = $this->translator->detectLanguage ();

            if ($this->isLanguageAvailable ($activeLanguage) === false)
                $activeLanguage = $this->getDefaultLanguage ();

            if ($this->isLanguageAvailable ($activeLanguage) === false)
                throw new IntegrityException ("The default language {$activeLanguage} cannot be found");

            return $activeLanguage;
        }
	};