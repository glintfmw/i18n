<?php declare(strict_types=1);
	namespace GlintFMW\I18N\Exception;

	/**
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMw\I18N\Exception
	 */
	class LocalizationMissingException extends \Exception
    {

    };