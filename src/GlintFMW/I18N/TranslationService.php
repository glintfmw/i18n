<?php declare (strict_types=1);
    namespace GlintFMW\I18N;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\I18N\Configuration\LocalizationProvider;
    use GlintFMW\I18N\Exception\LocalizationMissingException;

    /**
     * Translation service, provides access to the translation system
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\I18N
     */
    class TranslationService
    {
        private LocalizationProvider $localizationProvider;
        private Injector $dependencyInjector;

        function __construct (LocalizationProvider $localizationProvider, Injector $dependencyInjector)
        {
            $this->localizationProvider = $localizationProvider;
            $this->dependencyInjector = $dependencyInjector;
        }

        /**
         * Translates the given message
         *
         * @param string $key Message key to translate
         * @param mixed ...$extrainfo Parameters for the translation
         *
         * @return string The translated message
         */
        function translate (string $key, ...$extrainfo)
        {
            try
            {
                $localization = $this->localizationProvider->getTranslation (
                    $this->localizationProvider->getCurrentLanguage (),
                    $key
                );

                return sprintf ($localization, ...$extrainfo);
            }
            catch (LocalizationMissingException $ex)
            {

            }

            return $key;
        }
    };