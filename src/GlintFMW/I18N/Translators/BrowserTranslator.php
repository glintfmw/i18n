<?php declare(strict_types=1);
	namespace GlintFMW\I18N\Translators;

	use GlintFMW\HTTP\Request;
    use GlintFMW\I18N\Configuration\LocalizationProvider;
	use GlintFMW\I18N\Translator;

	/**
	 * Example translator that parses the UserAgent and tries to guess the language from it
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\I18N\Translators
	 */
	class BrowserTranslator implements Translator
	{
	    private LocalizationProvider $localizationProvider;
	    /** @var Request The request to be used to detect the language */
	    private Request $request;

	    function __construct (LocalizationProvider $localizationProvider)
        {
            $this->localizationProvider = $localizationProvider;
        }

        public function setRequest (Request $request): void
        {
            $this->request = $request;
        }

        /** @return string The current language to be used for the localization system */
		function detectLanguage (): string
		{
		    $headers = $this->request->getHeaders ();

		    if ($headers->has ('Accept-Language') == false)
		        return '';

			$clientSupportedLanguages = $headers->get ('Accept-Language');
			$langDescriptions = explode (',', $clientSupportedLanguages);
			$clientParsedLanguageList = array ();

			// parse the language strings
			// and make sure they are in the correct order and format
			foreach ($langDescriptions as $langDescription)
			{
				$qualityPos = stripos ($langDescription, "q=");

				if ($qualityPos !== false)
				{
					$quality = (float) trim (substr ($langDescription, $qualityPos + 2, strlen ($langDescription) - $qualityPos - 2));
					$lang = substr ($langDescription, 0, $qualityPos);
				}
				else
				{
					$quality = (float) 1;
					$lang = $langDescription;
				}

				// build an integer quality number to access the array correctly
				$quality = (int) ($quality * 100);

				$lang = trim ($lang);

				$separator = stripos ($lang, "-");

				if ($separator !== false)
				{
					$lang = substr ($lang, 0, $separator);
				}

				if (array_key_exists ($quality, $clientParsedLanguageList) == false)
				{
					$clientParsedLanguageList [$quality] = array ($lang);
				}
				else
				{
					$clientParsedLanguageList [$quality] [] = $lang;
				}
			}

			// after parsing the list time to look, taking in account gravity
			// and get the most fitting language possible
			krsort ($clientParsedLanguageList);

			foreach ($clientParsedLanguageList as $quality => $langList)
			{
				foreach ($langList as $lang)
				{
				    if ($this->localizationProvider->isLanguageAvailable ($lang) === true)
					{
						return $lang;
					}
				}
			}

			return '';
		}
	};
